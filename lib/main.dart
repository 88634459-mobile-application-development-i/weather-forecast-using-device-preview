import 'package:device_preview/device_preview.dart';
import 'package:example_template/aspect_ratio_example.dart';
import 'package:flutter/material.dart';

String app_theme = "dark";

void main() {
  runApp(contactProfilePage());
}
class MyAppTheme {
  static ThemeData appThemeLight() {
    return ThemeData(
      brightness: Brightness.light,
      appBarTheme: AppBarTheme(
        color: Colors.white,
        iconTheme: const IconThemeData(
          color: Colors.cyan,
        ),
      ),
      listTileTheme: ListTileThemeData(
        iconColor: Colors.cyan,
      ),
      iconTheme: const IconThemeData(color: Colors.cyan),
    );
  }

  static ThemeData appThemeDark() {
    return ThemeData(
      brightness: Brightness.dark,
      appBarTheme: AppBarTheme(
        color: Color.fromRGBO(48, 48, 48, 1),
        iconTheme: const IconThemeData(
          color: Colors.cyanAccent,
        ),
      ),
      listTileTheme: ListTileThemeData(
        iconColor: Colors.cyanAccent,
      ),
      iconTheme: const IconThemeData(color: Colors.cyanAccent),
    );
  }
}
class contactProfilePage extends StatefulWidget {
  @override
  State<contactProfilePage> createState() => _contactProfilePageState();
}

class _contactProfilePageState extends State<contactProfilePage> {
  var currentTheme = app_theme;

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
        tools: const [
        DeviceSection(),
    ],
    builder: (context) => MaterialApp(
    debugShowCheckedModeBanner: false,
    useInheritedMediaQuery: true,
    builder: DevicePreview.appBuilder,
    locale: DevicePreview.locale(context),
    title: 'Responsive and adaptive UI in Flutter',
    theme: ThemeData(
    //primarySwatch: Colors.green,
    ),
    home: Scaffold(
        appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            setState(() {
              currentTheme = currentTheme == "dark" ? "light" : "dark";
            });
          },
          child: Icon(Icons.refresh),
        ),
      ),
    ),
    );
  }
}

Widget buildButton(iconName, iconText) {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          iconName,
          size: 30,
        ),
        onPressed: () {},
      ),
      Text(iconText),
    ],
  );
}

Widget buildListTile(leadingIconName, titleText, subTitleText) {
  return ListTile(
    minLeadingWidth: 0,
    leading: Icon(leadingIconName),
    title: Text(titleText,
        style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
    subtitle: Text(subTitleText),
  );
}

//Text(subTitleText)
PreferredSizeWidget buildAppBarWidget() {
  return AppBar(
    title: Text("Weather Forecast"),
    elevation: 0,
    leading: const Icon(Icons.arrow_back),
    actions: <Widget>[
      IconButton(onPressed: () {}, icon: const Icon(Icons.calendar_month)),
    ],
  );
}

ListView buildBodyWidget() {
  return ListView(
    shrinkWrap: true,
    children: <Widget>[
      Column(
        children: <Widget>[
          SizedBox(
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Text("Seoul", style: TextStyle(fontSize: 30))),
              ],
            ),
          ),
          SizedBox(
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Text("-13°C",
                        style: TextStyle(
                            fontSize: 60,
                            color: Colors.cyan,
                            fontWeight: FontWeight.bold))),
              ],
            ),
          ),
          SizedBox(
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(left: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.only(left: 10),
                          child: Text("Mostly sunny",
                              style: TextStyle(fontSize: 15))),
                      IconButton(onPressed: () {}, icon: Icon(Icons.sunny))
                    ],
                  ),
                ),
              ],
            ),
          ),
          const Divider(
            thickness: 1,
          ),
          Container(
            margin: const EdgeInsets.only(top: 8, bottom: 8),
            child: Theme(
                data: ThemeData(
                  iconTheme: const IconThemeData(color: Colors.cyan),
                ),
                child: profileActionItems()),
          ),
          const Divider(
            thickness: 1,
          ),
          Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text("Sa",
                      style:
                      TextStyle(fontSize: 30, fontWeight: FontWeight.bold)),
                  buildButton(Icons.cloud, "Mostly  sunny"),
                  Text("-13°C",
                      style:
                      TextStyle(fontSize: 20, )),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text("Su",
                      style:
                      TextStyle(fontSize: 30, fontWeight: FontWeight.bold)),
                  buildButton(Icons.sunny, "        Sunny        "),
                  Text("-12°C",
                      style:
                      TextStyle(fontSize: 20, )),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text("Mo",
                      style:
                      TextStyle(fontSize: 30, fontWeight: FontWeight.bold)),
                  buildButton(Icons.cloud, "Mostly sunny"),
                  Text("-11°C",
                      style:
                      TextStyle(fontSize: 20, )),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text("Tu ",
                      style:
                      TextStyle(fontSize: 30, fontWeight: FontWeight.bold)),
                  buildButton(Icons.cloud, "Mostly sunny"),
                  Text("-9°C",
                      style:
                      TextStyle(fontSize: 20, )),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text("We",
                      style:
                      TextStyle(fontSize: 30, fontWeight: FontWeight.bold)),
                  buildButton(Icons.cloud, "Mostly sunny"),
                  Text("-9°C",
                      style:
                      TextStyle(fontSize: 20, )),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text("Th",
                      style:
                      TextStyle(fontSize: 30, fontWeight: FontWeight.bold)),
                  buildButton(Icons.sunny, "        Sunny        "),
                  Text("-7°C",
                      style:
                      TextStyle(fontSize: 20, )),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text("Fr",
                      style:
                      TextStyle(fontSize: 30, fontWeight: FontWeight.bold)),
                  buildButton(Icons.cloud, "Mostly sunny"),
                  Text("-7°C",
                      style:
                      TextStyle(fontSize: 20, )),
                ],
              )
            ],
          )
        ],
      )
    ],
  );
}

Widget profileActionItems() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: <Widget>[
      buildButton(Icons.south_east, "3 km/h"),
      buildButton(Icons.water_drop, "1 %"),
      buildButton(Icons.air, "Fair"),
    ],
  );
}
